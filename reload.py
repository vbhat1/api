import os
from flask import request, Flask
app = Flask(__name__)
@app.route('/reload')
def reload():
    if request.args.get('password') == 'D1g1talOcean':
        try:
            os.system('git pull origin master')
            return 'success'
        except:
            return 'fail'
    else:
        return 'wrong password'
